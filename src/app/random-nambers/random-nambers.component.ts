import {Component } from '@angular/core';


@Component({
  selector: 'app-random-nambers',
  templateUrl: './random-nambers.component.html',
  styleUrls: ['./random-nambers.component.css']
})


export class RandomNambersComponent {

  constructor() {
    const container = document.createElement('div');
    const btn = document.createElement('button');
    container.style.cssText = `
      background: cornflowerblue;
      max-width: 100%;
      padding: 60px;
      border: 2px solid rebeccapurple;
      font-weight: bold;
      text-align: center;
      color: rebeccapurple`
    btn.innerHTML = 'new numbers'
    container.append(btn)
    document.body.append(container)
    btn.addEventListener('click', () => {

      for (let i = 1; i <= 5; i++) {
        let massNum = Math.floor(Math.random() * this.generateNam().length);
        let massNumStr = String(massNum);
        this.addNumToPage(massNumStr);

      }
    })
  }

  generateNam() {
    return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36]
  }

  addNumToPage(num:string){
    const circle = document.createElement('div');
    circle.style.cssText = `background: gold; border: 1px solid red; border-radius: 50%; display: inline-block; padding: 20px; margin:10px`;
    circle.innerHTML = num;
    const cont = document.getElementById('cont')!;
    cont.append(circle);

  }
}
