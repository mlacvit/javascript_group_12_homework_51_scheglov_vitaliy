import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RandomNambersComponent } from './random-nambers.component';

describe('RandomNambersComponent', () => {
  let component: RandomNambersComponent;
  let fixture: ComponentFixture<RandomNambersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RandomNambersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RandomNambersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
