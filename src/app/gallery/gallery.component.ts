import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent {
  nameImage = '';
  urlImage = '';

  onAddToPage(event: Event){
    event.preventDefault();
    const gallery = document.getElementById('gallery')!;
    const titlePhoto = document.createElement('h3');
    titlePhoto.style.cssText = `display: inline-block`
    const carryover = document.createElement('br');
    titlePhoto.innerHTML = `${this.nameImage}`
    const photo = document.createElement('img');
    photo.src = `${this.urlImage}`;
    photo.style.cssText = `width: 200px; height: 200px; display: inline-block`;
    gallery.append(photo);
    gallery.append(carryover);
    gallery.append(titlePhoto);

    this.nameImage = '';
    this.urlImage = '';
  }
  disabledOn(){
    return this.nameImage === '' || this.urlImage === '';
  }
}
